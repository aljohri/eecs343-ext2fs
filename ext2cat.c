#include <unistd.h>
#include <stdio.h>

#include "ext2_access.h"
#include "mmapfs.h"


int main(int argc, char ** argv) {
    // Extract the file given in the second argument from the filesystem image
    // given in the first.
    if (argc != 3) {
        printf("usage: ext2cat <filesystem_image> <path_within_filesystem>\n");
        exit(1);
    }
    char * fs_path = argv[1];
    char * file_path = argv[2];
    void * fs = mmap_fs(fs_path);

    // Get the inode for our target file.
    __u32 target_ino_num = get_inode_by_path(fs, file_path);
    if (target_ino_num == 0) {
        printf("cat: %s does not exist\n", file_path);
        return 1;
    }
    struct ext2_inode * target_ino = get_inode(fs, target_ino_num);

    __u32 bytes_read, bytes_left, bytes_to_read;
    __u32 block_size = get_block_size(fs);
    __u32 size = target_ino->i_size;
    void * buf = calloc(size, 1);

    bytes_read = 0;


    /* Original For Loop */

    // for (int i = 0; i < EXT2_NDIR_BLOCKS; i++) {
    //     bytes_left = size - bytes_read;
    //     if (bytes_left == 0) break;
    //     bytes_to_read = bytes_left > block_size ? block_size : bytes_left;
    //     void * block = get_block(fs, target_ino->i_block[i]);
    //     memcpy(buf + bytes_read, block, bytes_to_read);
    //     bytes_read += bytes_to_read;
    // }
    
    /* Extra Credit Below */

    for (int i = 0; i < EXT2_N_BLOCKS-1; i++) {
        if (bytes_left == 0) break;
        
        if (i < EXT2_NDIR_BLOCKS) {
            void * block = get_block(fs, target_ino->i_block[i]);
            bytes_left = size - bytes_read;
            bytes_to_read = bytes_left > block_size ? block_size : bytes_left;
            memcpy(buf + bytes_read, block, bytes_to_read);
            bytes_read += bytes_to_read;
        }
        else if (i == EXT2_NDIR_BLOCKS) {
            void * block = get_block(fs, target_ino->i_block[i]);

            for(__u32 j = 0; j < block_size/sizeof(__u32); j++) {
                void * indir_block = get_block(fs, *(__u32 *)(block + (j * sizeof(__u32))));
                bytes_left = size - bytes_read;
                bytes_to_read = bytes_left > block_size ? block_size : bytes_left;
                memcpy(buf + bytes_read, indir_block, bytes_to_read);
                bytes_read += bytes_to_read;
            }
            
        }
        else if (i == EXT2_DIND_BLOCK) { 
            
            void * block = get_block(fs, target_ino->i_block[i]);

            for(__u32 j = 0; j < block_size/sizeof(__u32); j++) {
                void * indir_block = get_block(fs, *(__u32 *)(block + (j * sizeof(__u32))));
                for(__u32 k = 0; k < block_size/sizeof(__u32); k++) {
                    void * d_indir_block = get_block(fs, *(__u32 *)(indir_block + (j * sizeof(__u32))));
                    bytes_left = size - bytes_read;
                    bytes_to_read = bytes_left > block_size ? block_size : bytes_left;                    
                    memcpy(buf + bytes_read, indir_block, bytes_to_read);
                    bytes_read += bytes_to_read;
                }
            }
            
        }
        else if (i == EXT2_TIND_BLOCK) { 
            /*
            void * block = get_block(fs, target_ino->i_block[i]);
            for(__u32 j = 0; j < block_size/sizeof(__u32); j++) {
                void * indir_block = get_block(fs, *(__u32 *)(block + (j * sizeof(__u32))));
                for(__u32 k = 0; k < block_size/sizeof(__u32); k++) {
                    void * d_indir_block = get_block(fs, *(__u32 *)(indir_block + (k * sizeof(__u32))));
                    for(__u32 l = 0; l < block_size/sizeof(__u32); l++) {
                        void * t_indir_block = get_block(fs, *(__u32 *)(d_indir_block + (l * sizeof(__u32))));                        
                        bytes_left = size - bytes_read;
                        bytes_to_read = bytes_left > block_size ? block_size : bytes_left;                        
                        memcpy(buf + bytes_read, indir_block, bytes_to_read);
                        bytes_read += bytes_to_read;
                    }
                }
            }
            */
        }

    }

    write(1, buf, bytes_read);
    if (bytes_read < size) {
        printf("%s: file uses indirect blocks. output was truncated!\n", argv[0]);
        printf("size: %d\n", size);
        printf("bytes_read: %d\n", bytes_read);
    }

    return 0;
}

