// ext2 definitions from the real driver in the Linux kernel.
#include "ext2fs.h"

// This header allows your project to link against the reference library. If you
// complete the entire project, you should be able to remove this directive and
// still compile your code.
#include "reference_implementation.h"

// Definitions for ext2cat to compile against.
#include "ext2_access.h"

#include "stdbool.h"
 

typedef struct ext2_inode ext2_inode;
typedef struct ext2_super_block ext2_super_block;
typedef struct ext2_group_desc ext2_group_desc;
typedef struct ext2_dir_entry_2 ext2_dir_entry_2;

//char ** split_path(char * path, int * levels);

///////////////////////////////////////////////////////////
//  Accessors for the basic components of ext2.
///////////////////////////////////////////////////////////

// Return a pointer to the primary superblock of a filesystem.
ext2_super_block * get_super_block(void * fs) {
    return fs + SUPERBLOCK_OFFSET;
}


// Return the block size for a filesystem.
__u32 get_block_size(void * fs) {
    ext2_super_block* superblock = get_super_block(fs);
    return EXT2_BLOCK_SIZE(superblock);
}

// Return a pointer to a block given its number.
// get_block(fs, 0) == fs;
void * get_block(void * fs, __u32 block_num) {
    return fs + block_num * get_block_size(fs);
}


// Return a pointer to the first block group descriptor in a filesystem. Real
// ext2 filesystems will have several of these, but, for simplicity, we will
// assume there is only one.
ext2_group_desc * get_block_group(void * fs, __u32 block_group_num) {
    //return get_block(fs, 2); // static (2 is the block num within the block group)

    // dynamic
    ext2_super_block* superblock = get_super_block(fs);
    return get_block(fs, block_group_num * EXT2_BLOCKS_PER_GROUP(superblock) + 2);
}


// Return a pointer to an inode given its number. In a real filesystem, this
// would require finding the correct block group, but you may assume it's in the
// first one.
ext2_inode * get_inode(void * fs, __u32 inode_num) {
    //static

    //dynamic
    ext2_super_block* superblock = get_super_block(fs);
    __u32 inodes_per_group = EXT2_INODES_PER_GROUP(superblock);

    __u32 block_group = (inode_num - 1) / inodes_per_group; 
    __u32 local_inode_num = (inode_num - 1) % inodes_per_group;

    ext2_group_desc * group_desc = get_block_group(fs, block_group);
    __u32 inode_table_block_id = group_desc->bg_inode_table;

    return get_block(fs, inode_table_block_id) + local_inode_num * EXT2_INODE_SIZE(superblock); 
    
}



///////////////////////////////////////////////////////////
//  High-level code for accessing filesystem components by path.
///////////////////////////////////////////////////////////

// Chunk a filename into pieces.
// split_path("/a/b/c") will return {"a", "b", "c"}.
//
// This one's a freebie.
char ** split_path(char * path, int * levels) {
    int num_slashes = 0;
    for (char * slash = path; slash != NULL; slash = strchr(slash + 1, '/')) {
        num_slashes++;
    }

    // Copy out each piece by advancing two pointers (piece_start and slash).
    char ** parts = (char **) calloc(num_slashes, sizeof(char *));
    char * piece_start = path + 1;
    int i = 0;
    for (char * slash = strchr(path + 1, '/');
         slash != NULL;
         slash = strchr(slash + 1, '/')) {
        int part_len = slash - piece_start;
        parts[i] = (char *) calloc(part_len, sizeof(char));
        strncpy(parts[i], piece_start, part_len);
        piece_start = slash + 1;
        i++;
        (*levels)++;
        //printf("%d", *levels);
    }
    // Get the last piece.
    parts[i] = (char *) calloc(strlen(piece_start) + 1, sizeof(char));
    strncpy(parts[i], piece_start, strlen(piece_start));
    return parts;
}


// Convenience function to get the inode of the root directory.
struct ext2_inode * get_root_dir(void * fs) {
    return get_inode(fs, EXT2_ROOT_INO);
}


// Given the inode for a directory and a filename, return the inode number of
// that file inside that directory, or 0 if it doesn't exist there.
// 
// name should be a single component: "foo.txt", not "/files/foo.txt".
__u32 get_inode_from_dir(void * fs, struct ext2_inode * dir, char * name) {

    //return _ref_get_inode_from_dir(fs, dir, name);
    ext2_super_block* superblock = get_super_block(fs);
    __u32 block_size = get_block_size(superblock);

    for(int i = 0; i < EXT2_N_BLOCKS-1; i++) {
        ext2_dir_entry_2 * current_dir;
        void * current_block;

        if (i < EXT2_NDIR_BLOCKS) {
            current_block = get_block(fs, dir->i_block[i]);
            current_dir = current_block;
        
            while((void*)current_dir < (current_block+block_size) && current_dir->rec_len)
            {
                if(!strncmp(current_dir->name,name,current_dir->name_len)) return current_dir->inode;
                current_dir = (void*)current_dir + current_dir->rec_len;
            }
        }
        else if (i == EXT2_NDIR_BLOCKS) {
            
            current_block = get_block(fs, dir->i_block[i]);

            void * current_indir_block;

            for(__u32 j = 0; j < block_size/sizeof(__u32); j++) {
                current_indir_block = get_block(fs, *(__u32 *)(current_block + (j * sizeof(__u32))));
                current_dir = current_indir_block;
            
                while((void*)current_dir < (current_block+block_size) && current_dir->rec_len)
                {
                    if(!strncmp(current_dir->name,name,current_dir->name_len)) return current_dir->inode;
                    current_dir = (void*)current_dir + current_dir->rec_len;
                }


            }
            
        }
        else if (i == EXT2_DIND_BLOCK) { return 0; }
        else if (i == EXT2_TIND_BLOCK) { return 0; }

    }

    return 0;
}


// Find the inode number for a file by its full path.
// This is the functionality that ext2cat ultimately needs.
__u32 get_inode_by_path(void * fs, char * path) {
    //return _ref_get_inode_by_path(fs, path);
    int levels = 1;
    char ** splitpath = split_path(path,&levels);
    //int levels = sizeof(splitpath)/sizeof(char*);
    bool success = true;

    ext2_inode * inode_dir = get_root_dir(fs);
    __u32 inode_num;

    for (int i = 0; i < levels; i++) {
        inode_num = get_inode_from_dir(fs, inode_dir, splitpath[i]);
        if (!inode_num) { success = false; break; }
        inode_dir = get_inode(fs, inode_num);
    }

    return success ? inode_num : 0;
    
    
}

